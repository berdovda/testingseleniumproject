﻿using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System;
using WebDriverExtensions;


namespace TestingSeleniumProject
{
    public class FilterPage
    {
        private IWebElement searchField => Browsers.getDriver.FindElement(By.Id("header-search"));

        private IWebElement searchButton => Browsers.getDriver.FindElement(By.ClassName("button2"));
        //[FindsBy(How = How.Id, Using = "glf-pricefrom-var")]
        private IWebElement priceFrom => Browsers.getDriver.FindElement(By.Id("glf-pricefrom-var"));

        //[FindsBy(How = How.ClassName, Using = "n-snippet-cell2")]
        private IList<IWebElement> products => Browsers.getDriver.FindElements(By.CssSelector(".n-snippet-cell2, .n-snippet-card2"), 10);

        //private IEnumerable<string> prodPrices = products.Select(item => item.FindElement(By.CssSelector(".price")).Text.Replace(" ₽", ""));
        //private IList<IWebElement> prodPrices => Browsers.getDriver.FindElement(By.CssSelector(".n-snippet-list")).FindElements(By.CssSelector(".price"));

        public void SetFromPrice(string str)
        {
            priceFrom.SendKeys(str);
        }

        public int CountElements()
        {
            return products.Count;
        }

        public void SearchProduct(string str)
        {
            searchField.SendKeys(str);
            searchButton.Click();
        }

        public string FirstElementText()
        {
            return products[0].FindElement(By.CssSelector(".n-snippet-cell2__title, .n-snippet-card2__title")).Text;
        }

        public bool IsSortedRight()
        {
            //IEnumerable<string> prodPrices = products.Select(item => item.FindElement(By.CssSelector(".price")).Text.Replace(" ₽", "").Replace(" ", ""));
            //var prPr = prodPrices.Select(item => item.Text.Replace(" ₽", "").Replace(" ", ""));
            //IEnumerable<string> prodPrices = products.Select(item => item.FindElement(Browsers.getDriver, By.ClassName("n-snippet-cell2__price"), 10).Text.Replace(" ₽", "").Replace(" ", ""));

            List<int> prices = new List<int>();
            for (int i = 0; i < products.Count(); i++)
            {
                var x = products[i].FindElement(Browsers.getDriver, By.CssSelector(".n-snippet-cell2__main-price-wrapper"), 10).Text;
                prices.Add(System.Convert.ToInt32((x.Replace(" ₽", "").Replace(" ", ""))));
            }

            int min = 0;
            foreach (int i in prices)
            {
                if (i < min)
                {
                    return false;
                }
                min = i;
            }
            return true;
        }

    }
 

}
