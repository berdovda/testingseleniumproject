using NUnit.Framework;
using OpenQA.Selenium;

namespace TestingSeleniumProject
{
    /// <summary>
    /// Drafts for A/T before SpecFlow usage
    /// </summary>
    [TestFixture]
    public class Tests : AutomationCore
    {
        [Test]
        public void Test1()
        {
            Browsers.LinkClick("������");
            Browsers.LinkClick("�����������");
            Browsers.LinkClick("��������� ��������");
            Browsers.LinkClick("��� �������");
            Pages.filterPage.SetFromPrice("20000");
            Browsers.LinkClick("Apple");
            Browsers.LinkClick("Samsung");
            Browsers.LinkClick("�������� ����������");
            int result = Pages.filterPage.CountElements();
            Assert.AreEqual(48, result);
            string productName = Pages.filterPage.FirstElementText();
            Pages.filterPage.SearchProduct(productName);
            string newProductName = Pages.filterPage.FirstElementText();
            Assert.AreEqual(productName, newProductName);

            //int result = Browsers.CountElements("n-snippet-cell2");
            //Assert.Pass();

        }
        [Test]
        public void Test2()
        {
            Browsers.LinkClick("������");
            Browsers.LinkClick("�����������");
            Browsers.LinkClick("�������� � Bluetooth-���������");
            Browsers.LinkClick("��� �������");
            Pages.filterPage.SetFromPrice("5000");
            Browsers.LinkClick("Beats");
            Browsers.LinkClick("�������� ����������");
            int result = Pages.filterPage.CountElements();
            Assert.AreEqual(19, result);
            string productName = Pages.filterPage.FirstElementText();
            Pages.filterPage.SearchProduct(productName);
            string newProductName = Pages.filterPage.FirstElementText();
            Assert.AreEqual(productName, newProductName);
        }

        [Test]
        public void Test3()
        {
            Browsers.LinkClick("������");
            Browsers.LinkClick("�����������");
            Browsers.LinkClick("��������� ��������");
            Browsers.LinkClick("�� ����");
            System.Threading.Thread.Sleep(5000);
            Assert.IsTrue(Pages.filterPage.IsSortedRight());
        }
    }
}