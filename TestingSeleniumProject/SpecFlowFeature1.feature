﻿Feature: SpecFlowFeature1
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers
	Test Cases (with some differences)

@mytag
Scenario: MarketOne
	Given I am on yandex.ru
	And I have entered "Маркет"
	And I have entered "Электроника"
	And I have entered "Мобильные телефоны"
	And I have entered "Все фильтры"
	And I have set value "20000" for pricefrom
	And I have selected "Samsung"
	And I have selected "Apple"
	When I press "Показать подходящие"
	Then The number of elements should be "48" on the screen
	Given I have remembered element
	When I have searched for saved element
	Then Found element is the same

@mytag
Scenario: MarketTwo
	Given I am on yandex.ru
	And I have entered "Маркет"
	And I have entered "Электроника"
	And I have entered "Наушники и Bluetooth-гарнитуры"
	And I have entered "Все фильтры"
	And I have set value "5000" for pricefrom
	And I have selected "Beats"
	When I press "Показать подходящие"
	Then The number of elements should be "18" on the screen
	Given I have remembered element
	When I have searched for saved element
	Then Found element is the same

@mytag
Scenario: MarketThree
	Given I am on yandex.ru
	And I have entered "Маркет"
	And I have entered "Электроника"
	And I have entered "Мобильные телефоны"
	When I choose sort "по цене"
	Then Elements are sorted by price