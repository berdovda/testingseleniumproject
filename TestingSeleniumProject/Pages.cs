﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingSeleniumProject
{
    public static class Pages
    {
        private static T getPages<T>() where T : new()
        {
            var page = new T();
            //https://github.com/SeleniumHQ/selenium/issues/4387
            //PageFactory.InitElements(Browsers.getDriver, page); 
            return page;
        }
        public static FilterPage filterPage
        {
            get { return getPages<FilterPage>(); }
        }

    }
}
