﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace TestingSeleniumProject
{
    [TestFixture]
    public class AutomationCore
    {
        IWebDriver driver;
        [SetUp]
        public void startTest()
        {
            Browsers.Init();
        }
        [TearDown]
        public void endtest()
        {
            Browsers.Close();
        }
    }
}
