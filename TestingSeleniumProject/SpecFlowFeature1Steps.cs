﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;

namespace TestingSeleniumProject
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        private string storedElement;

        [BeforeFeature()]
        public static void Hook()
        {
            Browsers.Init();
        }

        [AfterFeature()]
        public static void UnHook()
        {
            Browsers.Close();
        }
        [Given(@"I am on yandex\.ru")]
        public void GivenIAmOnYandex_Ru()
        {
            Browsers.Goto("http://yandex.ru/");
        }
        
        [Given(@"I have entered ""(.*)""")]
        public void GivenIHaveEntered(string p0)
        {
            Browsers.LinkClick(p0);
        }
        
        [Given(@"I have set value ""(.*)"" for pricefrom")]
        public void GivenIHaveSetValueForPricefrom(string p0)
        {
            Pages.filterPage.SetFromPrice(p0);
        }
        
        [Given(@"I have selected ""(.*)""")]
        public void GivenIHaveSelected(string p0)
        {
            Browsers.LinkClick(p0);
        }

        [Given(@"I have remembered element")]
        public void GivenIHaveRememberedElement()
        {
            storedElement = Pages.filterPage.FirstElementText();
        }


        [When(@"I press ""(.*)""")]
        public void WhenIPress(string p0)
        {
            Browsers.LinkClick(p0);
        }
        
        [When(@"I have searched for saved element")]
        public void WhenIHaveSearchedForElement()
        {
            Pages.filterPage.SearchProduct(storedElement);
        }
        
        [When(@"I choose sort ""(.*)""")]
        public void WhenIChooseSortByPrice(string p0)
        {
            Browsers.LinkClick(p0);
            System.Threading.Thread.Sleep(5000);
        }
        
        [Then(@"The number of elements should be ""(.*)"" on the screen")]
        public void ThenTheNumberOfElementsShouldBeOnTheScreen(int p0)
        {
            Assert.AreEqual(p0, Pages.filterPage.CountElements());
        }
        
        [Then(@"Found element is the same")]
        public void ThenFoundElementIsTheSame()
        {
            Assert.AreEqual(storedElement, Pages.filterPage.FirstElementText());
        }
        
        [Then(@"Elements are sorted by price")]
        public void ThenElementsAreSortedByPrice()
        {
            Assert.IsTrue(Pages.filterPage.IsSortedRight());
        }
    }
}
